# CreetGame

CreetGame "Cross the street game" is a simple 2D game, which is similar to Frogger game. 


## Clone project:
```
git clone https://gitlab.com/os-rooney/creetgame.git
```
## Game preview
<img src="src/img/Creet-Game.gif" style="width:30rem">

## Design Pattern
<img src="src/img/designPattern.jpg" style="width:30rem">

## Game rules
- you must avoid being hit by a car, otherwise it will hurt.
